import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import Exercise1 from './Exercise1'
import Exercise2 from './Exercise2'

class RouterEx extends Component {
    render() {
        return (
            <NativeRouter>
                <Switch>
                    <Route exact path="/Exercise1" component={Exercise1} />
                    <Route exact path="/Exercise2" component={Exercise2} />
                    <Redirect to="/Exercise1"/>
                </Switch>
            </NativeRouter>
        )
    }
}
export default RouterEx