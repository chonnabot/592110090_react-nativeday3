import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import App1 from './App1'
import App2 from './App2'

class Router extends Component {
    render() {
        return (
            <NativeRouter>
                <Switch>
                    <Route exact path="/App1" component={App1} />
                    <Route exact path="/App2" component={App2} />
                    <Redirect to="/App1"/>
                </Switch>
            </NativeRouter>
        )
    }
}
export default Router