/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App1 from './App1';
import App2 from './App2';
import Router from './Router'

import Exercise1 from './Exercise1';
import Exercise2 from './Exercise2';
import RouterEx from './RouterEx'

import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => RouterEx);
