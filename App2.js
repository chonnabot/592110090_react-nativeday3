import React, { Component } from 'react'
import { View, Text, Button } from 'react-native'
import { Link } from 'react-router-native'
class App2 extends Component {
    goToScreen1 = () => {
        this.props.history.push('/App1',{ mynumber: 20 })
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#FFFF00' }}>
                <Text>Screen 2</Text>

                <Button title="Go Go ~" onPress={this.goToScreen1} />
            </View>
        )
    }
}
export default App2