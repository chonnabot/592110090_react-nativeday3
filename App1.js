import React, { Component } from 'react'
import { View, Text, Alert } from 'react-native'
import { Link } from 'react-router-native'

class App1 extends Component {
  UNSAFE_componentWillMount(){
    console.log(this.props)
    if(this.props.location && this.props.location.state && this.props.location.state.mynumber){
      Alert.alert('Your number is',this.props.location.state.mynumber+'')
    }
  }
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#7FFF00' }}>
        <Text >Screen 1</Text>
        <Link to="/App2">
            <Text >Go to Screen 2</Text>
        </Link>
      </View>
    )
  }
}
export default App1