import React, { Component } from 'react'
import { View, Text, Button, StyleSheet, TouchableOpacity, Alert } from 'react-native'
import { Link } from 'react-router-native'
class Exercise2 extends Component {
    goToScreen1 = () => {
        this.props.history.push('/App1', { mynumber: 20 })
    }
    UNSAFE_componentWillMount() {
        console.log(this.props)
        if (this.props.location && this.props.location.state && this.props.location.state.mymember) {
            // Alert.alert('Your number is', this.props.location.state.mymember + '')
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.headerBox1}>
                        <Text style={styles.Text} onPress={this.goToScreen1}> / </Text>
                    </View>
                    <View style={styles.headerBox2}>
                        <Text style={styles.Text}> My Profile </Text>
                    </View>
                </View>
                <View style={styles.body}>
                    <Text style={styles.Text}>Username</Text>
                    <Text style={styles.Text}>{this.props.location.state.mymember}</Text>
                    <Text style={styles.Text}>First name</Text>
                    <Text style={styles.Text}>Last name</Text>
                </View>
                <View style={styles.footer}>
                    <TouchableOpacity>
                        <Button height title="Edit" />
                    </TouchableOpacity>
                </View>
                {/* <Button title="Go Go ~" onPress={this.goToScreen1} /> */}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#666',
        flex: 1
    },
    Text: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 10,
    },
    header: {
        backgroundColor: 'white',
        flexDirection: 'row',
        flex: 1
    },
    headerBox1: {
        backgroundColor: '#00BFFF',
        flex: 1,
        margin: 2,
        marginLeft: -1,
        justifyContent: 'center',  //เลื่อนลงตาม row บนมาล่าง
        alignItems: 'center', // เลื่อน ซ้ายไปขวา
    },
    headerBox2: {
        backgroundColor: '#00BFFF',
        flex: 4,
        margin: 2,
        marginLeft: -1,
    },
    column: {                   //row
        backgroundColor: '#666',
        flexDirection: 'column',
        alignItems: 'center',
        flex: 1    //justifyContent

    },
    row: {                      //column
        backgroundColor: '#666',
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1     //justifyContent

    },
    image: {
        backgroundColor: 'white',
        width: 200,
        height: 200,
        borderRadius: 100,
    },
    radiusText: {
        fontSize: 20,
        fontWeight: 'bold',
        padding: 70,
    },
    body: {
        backgroundColor: '#666',
        flex: 8
    },
    box1: {
        backgroundColor: 'white',
        flexDirection: 'column',
        alignItems: 'center',
        width: 300,
        height: 60,
        borderRadius: 10,
        margin: 14,
    },
    box2: {
        backgroundColor: '#330000',
        flexDirection: 'column',
        alignItems: 'center',
        width: 300,
        height: 60,
        borderRadius: 10,
        margin: 14,
    },
    boxText1: {
        fontSize: 20,
        fontWeight: 'bold',
        padding: 10,
    },
    boxText2: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 10,
    },
    footer: {
        backgroundColor: '#00BFFF',
        flex: 1
    },
});

export default Exercise2